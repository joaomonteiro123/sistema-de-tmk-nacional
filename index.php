 <?php
error_reporting(E_WARNING);
session_start();
include "servicos/conexao.php"
?>

<HTML>
<HEAD>
    <TITLE>IIPC BH - TMK</TITLE>
    <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/estilo.css">
    <script src="js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/combobox.js"></script>
    <script src="js/paginathing.js"></script>


    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .form-center{   
            width: 20%;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
        .form-radius{  
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

        fieldset.scheduler-border {   
            text-align: center;
            background-color: #f7f9fc;
            min-width: 200px;             
            padding: 10px !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
                    box-shadow:  0px 0px 0px 0px #000;
        }
    </style>

    <script>
    </script>
</HEAD>

<BODY class="bg-light">  
    <?php 
        include "cabecalho/cabecalho.php";
    ?>

    <div id="div_corpo">
        <?php 
            include "home/home.php";
        ?>
    </div>

    <?php 
        include "rodape/rodape.php";
    ?>      
    <script src="js/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>