<?php
error_reporting(E_WARNING);
session_start();
?>

<HTML>
<HEAD>
    <TITLE>IIPC BH - TMK</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script>
    </script>
</HEAD>

<BODY class="bg-light">  
    <?php 
        include "cabecalho_login.php";
    ?>

    <div id="div_corpo" style="padding-top: 50px;">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto pl-4 pr-4">
        <div class="card card-signin my-5 ml-4 mr-4">
          <div class="card-body">
            <h5 class="card-title text-center">Acessar o Sistema</h5>
            <form class="form-signin" id="form_login">
              <div class="form-label-group">
                <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuário" required autofocus>
                <label for="inputEmail">Usuário</label>
              </div>

              <div class="form-label-group">
                <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" required>
                <label for="inputPassword">Senha</label>
              </div>

              <div class="wrapper">
                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">ENTRAR</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

    <?php 
        include "../rodape/rodape.php";
    ?>      
    <script src="../js/popper.min.js" crossorigin="anonymous"></script>
    <script src="login.js" crossorigin="anonymous"></script>
    <script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>