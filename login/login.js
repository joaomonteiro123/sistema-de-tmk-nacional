(function () { 
    'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('form-signin');
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                event.stopPropagation();
                form.classList.add('was-validated');
                }else{
                    form.classList.add('was-validated');
                    $.ajax({
                        url: "../servicos/login.php",
                        type: "post",
                        data: $("#form_login").serialize(),
                        success: function(resultado){
                            if (resultado == 'logado'){
                                window.location.replace('../index.php');
                            }else{
                                alert(resultado);
                            }
                        }
                    })
                }
                return 0;
            }, false);
            });
        }, false);
    })();
    