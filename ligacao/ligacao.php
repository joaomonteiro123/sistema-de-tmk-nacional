<div class="pl-4 pr-4 mr-4 ml-4">
<br/><br/><br/><br/>

    <div class="row">
        <div class="col-md-8 pr-2 pt-0">
            <div class="bg-azul-principal p-1 rounded">
                <div class="row mr-0 ml-0 pt-1 bg-azul-secundario rounded">
                    <div class="row m-3">
                        <b><font color="white">Cód. XXXX - NOME NOME NOME NOME, XX anos</font></b>
                    </div>
                    <table class="ml-1 mr-1 mb-0 table table-striped tabela-paginada small table-bordered rounded">
                        <!-- tabela-sem-borda-externa table table-bordered small -->
                        <!-- table table-striped tabela-paginada small table-bordered rounded -->
                        <tbody>
                            <tr>
                                <td><font<b>Telefones: </b> 31 1111-1111</font></td>
                                <td><font><b>Profissão: </b> Catador de Coquinho</font></td>
                            </tr>
                            <tr>
                                <td><font><b>Primeira vez no IIPC: </b> 01/05/2001</font></td>
                                <td><font><b>Cidade: </b> Contagem</font></td>
                            </tr>
                            <tr>
                                <td><font><b>Última vez no IIPC: </b> 25/05/2018</font></td>
                                <td><font><b>Email: </b> nome@email.com</font></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="small pt-2 pl-2 pb-1">
                        <a class="text-white">Últimas Ligações</a>
                    </div>
                    <table id="tbl_ligacoes" class="mb-0 mr-1 ml-1 mt-1 table table-striped tabela-paginada small table-bordered">
                        <thead class="thead-light">
                            <tr>
                                <td><b>Data</b></td>
                                <td><b>Resultado</b></td>
                                <td><b>Voluntário</b></td>
                                <td><b>Comentários</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>20/07/18 - 10:50</td>
                                <td><font color="red">Não Atendeu</font></td>
                                <td>Claudia Marisa</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>14/08/18 - 15:32</td>
                                <td><font color="green">Atendeu</font></td>
                                <td>Vitória Alipio</td>
                                <td>Disse que gosta de maçãs</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><font color="green">3</font></td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><font color="green">4</font></td>
                                <td>4</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><font color="green">5</font></td>
                                <td>5</td>
                                <td>5</td>
                            </tr>
                        </tbody>
                    </table>
                    <span id="page_ligacoes"></span>
                    <div class="small pt-2 pl-2 pb-1">
                        <a class="text-white">Comentários IIPC Net</a>
                    </div>
                    <div id="collapse" class="in col-md-12 pt-1 pb-1 pl-1 pr-1 small">
                        <div class="bg-light p-2 rounded">CIP/16--Pacifismologia/18--@--Em 17-04-17- Convidado para o GVI, não poderá fazer cursos no momento, e somente no segundo semestre de 2017. Rosilene// Confirmado ECP2 2017 Jamel// Em 02.12 convidado para o PDP, tem interesse.EM 27/01/18 TEL NÃO ATENDEU, DIVULGAÇÃO POR EMAIL DO LABORATÓRIO DE TÉCNICAS ENERGÉTICAS.JULIANA//</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 pt-0 pl-2">
            <div class="ml-0 mr-0 mb-1 mt-0 row bg-azul-secundario rounded">
                <a class="w-100 pt-2 text-white text-center small"><b>Cursos Realizados</b></a>
                <input class="w-75 mt-1 ml-1 rounded p-1" type="text" placeholder="Pesquisar" aria-label="Search">
                <table id="tbl_cursos" class="mb-0 mr-1 ml-1 mt-1 table table-striped tabela-paginada small table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <td><b>Data</b></td>
                            <td><b>Curso</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>20/07/18</td>
                            <td>PALESTRA POPULAR GRATUITA</td>
                        </tr>
                        <tr>
                            <td>14/08/18</td>
                            <td>CIP - CURSO INTEGRADO DE PROJECIOLOGIA E CONSCIENCIOLOGIA</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <span id="page_cursos"></span>
            </div>
            <div class="ml-0 mr-0 mb-0 mt-1 row bg-azul-secundario rounded">
                <div class="text-center p-1 w-50 p-0 text-white small"><b>Cursos de Interesse</b></div>
                <div class="text-center p-1 w-50 p-0 text-white small"><b>Temas de Interesse</b></div>
                <table class="m-1 table table-striped tabela-paginada small table-bordered rounded">
                    <tbody>
                        <tr>
                            <td class="w-50">PALESTRA POPULAR GRATUITA</td>
                            <td class="w-50">Experiência fora do corpo</td>
                        </tr>
                        <tr>
                            <td>CIP - CURSO INTEGRADO DE PROJECIOLOGIA E CONSCIENCIOLOGIA</td>
                            <td>Calopsitas</td>
                        </tr>
                        <tr>
                            <td>CL - (em estudo)</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="ligacao/ligacao.js" crossorigin="anonymous"></script>
