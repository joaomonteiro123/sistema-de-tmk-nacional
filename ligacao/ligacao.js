$(document).ready(function(){
    //Paginação das tabelas
    $('#tbl_ligacoes tbody').paginathing({
        perPage: 5,
        limitPagination: 10,
        firstText: 'Início',
        lastText: 'Final',
        containerClass: 'ml-1 mr-1 mb-1 pagination-container d-flex justify-content-center w-100 bg-light',
        ulClass: 'pagination mt-2 mb-1',
        liClass: 'page-item',
        activeClass: 'active',
        disabledClass: 'disabled',
        insertAfter: ('#page_ligacoes')
    })
    $('#tbl_cursos tbody').paginathing({
        perPage: 5,
        limitPagination: 5,
        firstText: 'Início',
        lastText: 'Final',
        containerClass: 'ml-1 mr-1 mb-1 pagination-container d-flex justify-content-center w-100 bg-light',
        ulClass: 'pagination mt-2 mb-1',
        liClass: 'page-item',
        activeClass: 'active',
        disabledClass: 'disabled',
        insertAfter: ('#page_cursos')
    })

    //
});