<nav class="navbar navbar-expand-md navbar-dark bg-azul-principal">
    <a class="navbar-brand">
        <img class="d-block mx-auto" id="img_home" src="images/logo-iipc-30anos.png" height="40px">
    </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" id="ligacao">
                <a class="nav-link pointer ml-3 mr-3"><b>Listas de TMK</b></a>
            </li>
            <li class="nav-item" id="ligacoes_pendentes">
                <a class="nav-link pointer ml-3 mr-3"><b>Ligações Pendentes</b></a>
            </li>
            <li class="nav-item" id="buscar_aluno">
                <a class="nav-link pointer ml-3 mr-3"><b>Buscar Aluno</b></a>
            </li>
            <li class="nav-item" id="acesso_admin">
                <a class="nav-link pointer ml-3 mr-3"><b>Acesso Administrador</b></a>
            </li>
        </ul>

        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link mr-sm-5" href="#">Voluntário</a>
            </li>
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="servicos/logout.php">Sair</a>
            </li>
        </ul>
    </div>
</nav>

<script src="cabecalho/cabecalho.js" crossorigin="anonymous"></script>