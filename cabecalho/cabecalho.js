$(document).ready(function () { 
    $(".nav-item").click( function (){
        pagina = $(this).attr('id');
        $(".nav-item").removeClass('active');
        $(this).addClass('active');
        $('#div_corpo').fadeOut('fast', function(){
            url = pagina+"/"+pagina+".php";
            $('#div_corpo').load(url, function(){
                $('#div_corpo').fadeIn('fast');
            });
        });
    });
    $("#img_home").click( function (){
        $(".nav-item").removeClass('active');
        $('#div_corpo').fadeOut('fast', function(){
            $('#div_corpo').load("home/home.php", function(){
                $('#div_corpo').fadeIn('fast');
            });
        });
    });
});